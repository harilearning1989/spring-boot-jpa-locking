package com.web.demo.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.web.demo.models.Employee;
import com.web.demo.records.EmployeeRecord;
import com.web.demo.repos.EmployeeRepository;
import com.web.demo.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    JsonUtil jsonUtil;

    @Autowired
    public EmployeeServiceImpl setJsonUtil(JsonUtil jsonUtil) {
        this.jsonUtil = jsonUtil;
        return this;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee updateEmployee(Long id, Employee employeeDetails) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new RuntimeException("Employee not found"));
        employee.setUsername(employeeDetails.getUsername());
        employee.setEmail(employeeDetails.getEmail());
        employee.setAge(employeeDetails.getAge());
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> saveAll() throws IOException {
        String employeeFile = "EmployeeData.json";
        List<EmployeeRecord> employeeRecords =
                jsonUtil.loadEntities(employeeFile, new TypeReference<List<EmployeeRecord>>() {});
        if (employeeRecords.isEmpty()) {
            System.out.println("No employees found or there was an error.");
        }
        List<Employee> employeeList = convertRecordToEntity(employeeRecords);
        employeeList = employeeRepository.saveAll(employeeList);
        return employeeList;
    }

    private List<Employee> convertRecordToEntity(List<EmployeeRecord> employeeRecords) {
        return employeeRecords
                .stream()
                .map(m -> {
                    Employee employee = Employee.builder()
                            .empName(m.name())
                            .username(m.username())
                            .age(m.age())
                            .email(m.email())
                            .gender(m.gender())
                            .phone(m.phone())
                            .build();
                    return employee;
                })
                .toList();
    }
}
